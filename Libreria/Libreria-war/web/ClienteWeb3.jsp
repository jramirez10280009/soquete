<%-- 
    Document   : ClienteWeb
    Created on : 4/11/2014, 07:54:26 AM
    Author     : CarlosAlberto
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" import="entidad.*, stateless.*, java.math.BigDecimal, javax.naming.*, java.util.*"%>
<%!
    private LibroBeanRemote librocat = null;
    String s1;
    Collection list;

    public void jspInit() {
        try {
            InitialContext context = new InitialContext();
            librocat = (LibroBeanRemote) context.lookup(LibroBeanRemote.class.getName());
            System.out.println("Cargando Catalogo Bean" + librocat);
        } catch (Exception ex) {
            System.out.println("error " + ex.getMessage());
        }
    }

    public void jspDestroy() {
        librocat = null;
    }
%>
<%
    try {
        s1 = request.getParameter("id");
       Libro elemento = librocat.buscalibro(Integer.parseInt(s1));
%>
<jsp:forward page="forma2.jsp">
    <jsp:param name="id" value="<%=elemento.getId()%>"/>
    <jsp:param name="t1" value="<%=elemento.getTitulo()%>"/>
    <jsp:param name="aut" value="<%=elemento.getAutor()%>"/>
    <jsp:param name="precio" value="<%=elemento.getPrecio()%>"/>
</jsp:forward>
<%
    response.flushBuffer();
%>    
<%
    } catch (Exception ex) {
        ex.printStackTrace();
    }
%>