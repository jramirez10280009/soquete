<%-- 
    Document   : ClienteWeb
    Created on : 4/11/2014, 07:54:25 AM
    Author     : Usuario
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" import="entidad.*, stateless.*, java.math.BigDecimal, javax.naming.*, java.util.*"%>
<%!
    private LibroBeanRemote librocat = null;
    String s0;
    Collection list;
    Libro libro;
    public void jspInit() {
        try {
            InitialContext context = new InitialContext();
            librocat = (LibroBeanRemote) context.lookup(LibroBeanRemote.class.getName());
            System.out.println("Cargando Catalogo Bean" + librocat);
        } catch (Exception ex) {
            System.out.println("error " + ex.getMessage());
        }
    }

    public void jspDestroy() {
        librocat = null;
    }
%>
<%
    try {
        s0 = request.getParameter("id");
        if (s0 != null) {
            int ids=Integer.parseInt(s0);
            librocat.eliminarlibro(ids);
            System.out.println("Libro Eliminado");
        }
%>
<title>Eliminar Libro</title>
<center>
<h1>Registro de Libros</h1>
<hr>
<p>Libro eliminado</p>
<table border="1" width="500">
    <tr align="center">
        <td>ID:</td>
        <td>Titulo:</td>
        <td>Autor:</td>
        <td>Precio:</td>
    </tr>
<%
    
    list = librocat.getLibro();
    for (Iterator iter = list.iterator(); iter.hasNext();) {
        Libro elemento = (Libro) iter.next();
%>
    <tr>
        <td align="center"><%=elemento.getId()%></td>
        <td><%=elemento.getTitulo()%></td>
        <td><%=elemento.getAutor()%></td>
        <td align="center"><%=elemento.getPrecio()%></td>
    </tr>
<%
    }
    response.flushBuffer();
%>
</table>
<p><a href="buscaid2.jsp">Eliminar Libro</a></p>
<p><a href="index.html">Inicio</a></p>
</center>       
<%
    } catch (Exception ex) {
        ex.printStackTrace();
    }
%>