/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package stateless;

import entidad.Libro;
import java.math.BigDecimal;
import java.util.Collection;
import javax.ejb.Remote;

/**
 *
 * @author CarlosAlberto
 */
@Remote
public interface LibroBeanRemote {
    public void AñadeLibros(String Titulo,String Autor,BigDecimal precio);
    public Collection <Libro> getLibro();
    public Libro buscalibro(int id);
    public void actualizarlibro(int id, String Titulo,String Autor,BigDecimal precio);
    public void eliminarlibro(int id);
}
