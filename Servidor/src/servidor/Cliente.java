/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package servidor;
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.net.*;
import java.io.*;

public class Cliente extends JFrame implements ActionListener{
    Socket cliente;
    DataInputStream flujo_entrada;
    DataOutputStream flujo_salida;
    //double num1,num2,resul;

    JPanel p1, p2, p3;
    JLabel l1, l2, l3;
    JTextField n1, n2;
    JButton b;
    JTextArea display;
    
    public Cliente()
    {
        super("Cliente de socket");
        p1=new JPanel();
        l1=new JLabel("Introduce por favor los valores");
        p1.add(l1);
        
        p2=new JPanel();
        l2=new JLabel("primer sumando");
        p2.add(l2);
        n1=new JTextField(5);
        p2.add(n1);
        l3=new JLabel("segundo sumando");
        p2.add(l3);
        n2=new JTextField(5);
        p2.add(n2);
        b=new JButton("Servidor dame suma");
        p2.add(b);
        
        p3=new JPanel();
        display=new JTextArea();
        p3.add(display);
        
        setSize(600,500);
        setVisible(true);
        setLayout(new GridLayout(3,1));
        add(p1);
        add(p2);
        add(p3);
        
        
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);//WindowsConstants.EXIT_ON_CLOSE
        b.addActionListener(this);
        
    }         

 @Override
    public void actionPerformed(ActionEvent e) {
       double s1,s2;    
    	s1=Double.parseDouble(n1.getText());
    	s2=Double.parseDouble(n2.getText());
    	System.out.println("Haciendo petición");
    	peticionServidor(s1,s2);
    }
    
    private void peticionServidor(double s1, double s2)
    {
        Socket client;
        DataInputStream input;
        DataOutputStream output;
         double suma;    
         String Suma;
         try {
                cliente = new Socket(InetAddress.getLocalHost(),6000);
		display.setText("Socket Creado....\n");
                // creación de las instancias para el flujo de datos
                //System.out.println("Socket creado");
                //System.out.println("Dame el primer numero");
                //num1=sc.nextDouble();
                //System.out.println("Dame el segundo numero");
                //num2=sc.nextDouble();
                flujo_entrada = new DataInputStream (cliente.getInputStream());
                flujo_salida = new DataOutputStream (cliente.getOutputStream());
                //System.out.println("Enviando primer numero");
	        display.append("Enviando primer sumando\n");
                flujo_salida.writeDouble(s1);
		display.append("Enviando segundo sumando\n");
                //System.out.println("Enviando segundo numero");
                flujo_salida.writeDouble(s2);
		display.append ("El servidor dice....\n\n");
                //System.out.println("El servidor dice:");
                suma=flujo_entrada.readDouble();
	        display.append("El  resultado es: "+ suma+"\n\n");
                display.append("Cerrando cliente\n\n");
                //System.out.println("El resultado de la suma es: "+suma);
                //System.out.println("Cerrando conexion");
                cliente.close();
              }

                catch(IOException e){
                 e.printStackTrace();
                }
     }
    
    public static void main(String args[])
    {
       new Cliente();
        
    }
}