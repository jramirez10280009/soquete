/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package servidor;
import java.awt.*;
import javax.swing.*;
import java.net.*;
import java.io.*;

public class Servidor extends JFrame{
    JTextArea display;
    ServerSocket server;
    Socket conexion;
    DataOutputStream flujo_salida;
    DataInputStream flujo_entrada;
    
      double s1,s2,suma;

    public Servidor()
    {
       display= new JTextArea(20,5);
       add("Center",display);
       setSize(400,300);
       setVisible(true);
       setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
       
   }

  public void runServer()
  {
     
try {     server=new ServerSocket(6000,100);
          //System.out.println("Servidor iniciado y en espera...");
          display.setText("Servidor iniciado y en espera....\n");
          conexion = server.accept();
          flujo_entrada= new DataInputStream(conexion.getInputStream());
          flujo_salida= new DataOutputStream(conexion.getOutputStream());
          //System.out.println("Cliente conectado");
          display.setText("Se ha aceptado una conexión....\n");
           //System.out.println("Se ha aceptado una conexión....\n");
          display.append("\nRecibiendo sumandos...\n");  
           //Creación de las intancias para la transferencia de información
	   //System.out.println("Recibiendo primer numero");
          display.append( "\nRecibiendo el primer sumando\n");
           s1= flujo_entrada.readDouble();
           //System.out.println("Recibiendo segundo numero");
           //s2= flujo_entrada.readDouble();
	  display.append( "\nRecibiendo el segundo sumando");
           s2 =flujo_entrada.readDouble();
                
           display.append("\nEnviando Resultado..\n");  
           //System.out.println("Haciendo suma");
           suma= s1+s2;
           //System.out.println("Enviando suma...");
           flujo_salida.writeDouble(suma);	   
           display.append( "\n Transmisión terminada.\n Se cierra el socket.\n ");
           System.out.println("Conexion cerrada");
           conexion.close();
           //server.close();
         }
      catch(IOException e){
         e.printStackTrace();
         }
      }

   public static void main( String args[]) 
      {
           Servidor s = new Servidor();
           s.runServer();
       }

}
